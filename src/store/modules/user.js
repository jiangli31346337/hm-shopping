import { getInfo, setInfo } from '@/utils/storage'

export default {
  namespaced: true,
  state () {
    return {
      userInfo: getInfo()
    }
  },
  mutations: {
    setUserInfo (state, userInfo) {
      state.userInfo = userInfo
      // 登录信息持久化到本地
      setInfo(userInfo)
    }
  },
  actions: {
    logout (store) {
      store.commit('setUserInfo', {})
      store.commit('cart/setCartList', [], { root: true })
    }
  },
  getters: {}
}
