import request from '@/utils/request'

// 商品搜索
export const getProList = (params) => {
  const { sortType, sortPrice, categoryId, goodsName, page } = params
  return request.get('/goods/list', {
    params: {
      sortType,
      sortPrice,
      categoryId,
      goodsName,
      page
    }
  })
}

// 获取商品详情
export const getProDetail = (goodsId) => {
  return request.get('/goods/detail', {
    params: {
      goodsId
    }
  })
}

// 获取商品评论
export const getProComments = (goodsId, limit = 3) => {
  return request.get('/comment/listRows', {
    params: {
      goodsId, limit
    }
  })
}
